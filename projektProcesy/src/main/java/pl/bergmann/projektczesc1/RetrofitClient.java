package pl.bergmann.projektczesc1;

import okhttp3.OkHttpClient;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class RetrofitClient {
    public Retrofit getRetrofitClient() {
        OkHttpClient httpClient = new OkHttpClient();
        return new Retrofit.Builder()
                .baseUrl("http://localhost:9090/register/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient)
                        .build();
    }
}