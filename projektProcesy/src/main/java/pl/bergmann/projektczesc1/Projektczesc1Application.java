package pl.bergmann.projektczesc1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projektczesc1Application {

    public static void main(String[] args) {
        SpringApplication.run(Projektczesc1Application.class, args);
    }

}
