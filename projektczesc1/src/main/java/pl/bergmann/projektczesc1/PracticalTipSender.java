package pl.bergmann.projektczesc1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PracticalTipSender {

    private static final Logger log = LoggerFactory.getLogger(PracticalTipSender.class);

    private final RabbitTemplate rabbitTemplate;

    public PracticalTipSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 3000L)
    public void sendPracticalTip() {
        ProcessOfComputer tip = new ProcessOfComputer("ala","ma","kota","a","kot",2,3,4,"dupa");
        rabbitTemplate.convertAndSend(Projektczesc1Application.EXCHANGE_NAME, Projektczesc1Application.ROUTING_KEY, tip);
        log.info("Practical Tip sent");
    }
}