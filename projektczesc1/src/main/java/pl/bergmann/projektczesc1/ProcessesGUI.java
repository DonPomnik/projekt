package pl.bergmann.projektczesc1;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Route
public class ProcessesGUI extends VerticalLayout
{
    private Grid<ProcessOfComputer> proceses;
    private TextArea pid;
    private Button buttonToSend;
    private Button refreshButton;
    List<ProcessOfComputer> s = new ArrayList<>();

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public ProcessesGUI()
    {
        this.refreshButton = new Button("Odśwież liste");
        this.proceses = new Grid<>(ProcessOfComputer.class);
        this.pid = new TextArea("PID procesu do zabicia");
        this.buttonToSend = new Button("Zabij Process");


        HorizontalLayout v = new HorizontalLayout();
        v.setWidthFull();
        v.add(proceses);

        buttonToSend.addClickListener(buttonClickEvent -> killProcess());
        refreshButton.addClickListener(buttonClickEvent -> refreshList());

        add(refreshButton,v,pid,buttonToSend);
    }

    private void killProcess()
    {
        try
        {
            String taskListExe = System.getenv("windir") + "\\system32\\" + "tasklist.exe";
            ProcessOfComputer killedProcess = new ProcessOfComputer();
            for(int i = 0;i<s.size();i++)
            {
                if(s.get(i).getPid() == Integer.parseInt(pid.getValue()))
                {
                    killedProcess = s.get(i);
                    Notification.show("Wykonano pomyślnie");

                }
            }

            if(killedProcess.getPid() == 0)
            {
                Notification.show("Nie ma takiego pid");
                return;
            }

            RetrofitClient retrofitClient = new RetrofitClient();
            RegisterService service = retrofitClient.getRetrofitClient().create(RegisterService.class);
            service.register(killedProcess).execute();

            String cmd = "taskkill /F /PID " + pid.getValue(); //tu wstawiasz pid prcoeseu ktorego chcesz zabic
            Runtime.getRuntime().exec(cmd);
        }

        catch (Exception err)
        {
            err.printStackTrace();
        }
    }

    private void refreshList()
    {

        try {
            String process;
            String taskListExe = System.getenv("windir") + "\\system32\\" + "tasklist.exe";
            Process p = Runtime.getRuntime().exec(new String[] { taskListExe, "/v" });

            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

            int i = 0;
            List<String> arr = new ArrayList<>();
            while ((process = input.readLine()) != null)
            {
                i++;

                //Start From Row where actuall process begins
                //Example
                //1
                //2Image Name                     PID Session Name        Session#    Mem Usage Status          User Name                                              CPU Time Window Title
                //3========================= ======== ================ =========== ============ =============== ================================================== ============ ========================================================================
                //4System Idle Process              0 Services                   0          4 K Unknown         NT AUTHORITY\SYSTEM                                    10:20:45 N/A
                if(i > 3)
                {
                    process = process.replaceAll("[^a-zA-Z0-9\\s:]+","");
                    System.out.println(i + process);
                    arr.add(process);
                    ProcessOfComputer processOnComputer = createProcess(process);
                    rabbitTemplate.convertAndSend("procesy1",processOnComputer);


                    s.add(processOnComputer);
                }
            }
            proceses.setItems(s);
            input.close();
        } catch (Exception err)
        {
            err.printStackTrace();
        }
    }

    private ProcessOfComputer createProcess(String process)
    {
        //substring bo dodaje K z memoryusage
        return new ProcessOfComputer(
            getName(process),getSessionName(process),getStatus(process).substring(1),getUserName(process),getWindowTitle(process)
                ,getPid(process),session(process),memUsage(process),cpuTime(process)
        );
    }




    private  String getName(String process){ return getProcessDetailString(process,0); }
    private  String getSessionName(String process){ return getProcessDetailString(process,1); }
    private  String getStatus(String process){ return getProcessDetailString(process,2); }
    private  String getUserName(String process){ return getProcessDetailString(process,3); }
    private  String getWindowTitle(String process){ return getWTitle(process); }
    private  int getPid(String process)
    {
        return getPrcoessDetail(process,0);
    }
    private  int session(String process)
    {
        return getPrcoessDetail(process,1);
    }
    private  int memUsage(String process)
    {
        return getPrcoessDetail(process,2);
    }
    private   String cpuTime(String process)
    {
        return getCpuTime(process);
    }


    ///
    /// 0 = pid | 1 = session | 2 = mem usage | 3 = cpu time
    ///
    private  int getPrcoessDetail(String process, int index)
    {
        if(checkLength(process))
        {
            //System.out.println("pos of exe is "  + process.indexOf("exe"));
            for(int i = 0;i<process.indexOf("exe") + 3; i++)
            {
                if(Character.isDigit(process.charAt(i)))
                {
                    process = process.replace(process.charAt(i),' ');
                }
            }
        }

        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(process);
        int i = 0;
        while(m.find())
        {
            if(i == index)
            {
                return Integer.parseInt(m.group());
            }
            i++;
        }

        return -1;
    }

    private  String getCpuTime(String process)
    {
        String cpuTime = "";
        for(int i = process.indexOf(':'); i<process.indexOf(':') + 6;i++)
        {
            if (i == process.indexOf(':'))
            {
                cpuTime += process.charAt(process.indexOf(':') - 1);
            }
            cpuTime += process.charAt(i);
        }
        return cpuTime;
    }

    private  String getWTitle(String process)
    {
        String getWindowTitle = "";
        for(int i = process.indexOf(':') + 6; i<process.length();i++)
        {
            getWindowTitle += process.charAt(i);
        }
        return getWindowTitle.replaceAll("\\s+","");
    }

    //Check whether name of process contains number
    private   boolean checkLength(String process)
    {
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(process);
        int i = 0;

        while(m.find())
        {
            i++;
        }

        if(i>4)
            return true;
        else
            return false;
    }

    private  String getProcessDetailString(String process, int index)
    {
        // First change
        //4System Idle Process              0 Services                   0          4 K Unknown         NT AUTHORITYSYSTEM                                     6:47:35 NA
        //          TO
        //
        process = process.replaceAll("\\s{2,}", "|").trim();

        //Netx change
        //SystemIdleProcess|0Services|0|4KUnknown|NTAUTHORITYSYSTEM|6:47:35NA|
        //      TO
        //SystemIdleProcess|Services||KUnknown|NTAUTHORITYSYSTEM|NA|

        process = process.replaceAll("[^A-Za-z|]","");
        process = process.replace("||","|");


        int counter = 0;
        for (String element:process.split("\\|"))
        {
            //  temp += element;
            // System.out.println(element);
            if(element.equals("K")) {
                continue;
            }
            if(counter == index)
                return element;
            counter++;
        }
        return "";
    }



}
