package pl.bergmann.projektczesc1;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RegisterService {
    @Headers({"Accept: application/json"})
   @POST("/register")
    Call<Void> register(@Body ProcessOfComputer processOfComputer);
}
