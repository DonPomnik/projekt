package pl.bergmann.projektczesc2;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoggerApiTests
{
    @Test
    public void contextLoads()
    {
        //given
        LoggerApi loggerApi=mock(LoggerApi.class);
        //when
        when(loggerApi.test()).thenReturn(test123());
        //then
        Assert.assertThat(loggerApi.test(),Matchers.hasSize(2));
    }

    public List<ProcessOfComputer> test123()
    {
        List<ProcessOfComputer> aaa = new ArrayList<>();
        ProcessOfComputer processOfComputer = new ProcessOfComputer("ala","ma","kota","anna","wa",1,2,3,"12:23:24");
        ProcessOfComputer processOfComputer123 = new ProcessOfComputer("kuta","ma","kota","anna","wa",1,2,3,"12:23:24");
        aaa.add(processOfComputer);
        aaa.add(processOfComputer123);
        return aaa;
    }



}
