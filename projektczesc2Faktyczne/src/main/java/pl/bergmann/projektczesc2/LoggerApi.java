package pl.bergmann.projektczesc2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LoggerApi
{
    ProcessOfComputerRepo repo;
    PracticalTipListener practicalTipListener;

    @Autowired
    public LoggerApi(ProcessOfComputerRepo repo, PracticalTipListener practicalTipListener)
    {
        this.repo = repo;
        this.practicalTipListener = practicalTipListener;
    }

    @PostMapping("/register")
    public String register(@RequestBody ProcessOfComputer processOfComputer)
    {
        //s
        boolean founded = false;

        if(practicalTipListener.processOfComputers.size() > 0)
        {
            List<ProcessOfComputer> currentProcesses = practicalTipListener.processOfComputers;
                for(int i = 0;i<currentProcesses.size();i++)
            {
               if(currentProcesses.get(i).getPid() == processOfComputer.getPid())
                   founded = true;
            }
        }
        if(founded)
        {
            repo.save(processOfComputer);
            return "zapisano";
        }
        else
        {
            return "nie zapisano";
        }
    }

    public List<ProcessOfComputer> test()
    {
        List<ProcessOfComputer> aaa = new ArrayList<>();
        ProcessOfComputer processOfComputer = new ProcessOfComputer("ala","ma","kota","anna","wa",1,2,3,"12:23:24");
        aaa.add(processOfComputer);
        return aaa;
    }

    public String testKolejny(String pid)
    {
        ProcessOfComputer processOfComputer = new ProcessOfComputer();
        try
        {
            processOfComputer.setPid(Integer.parseInt(pid));
            return "Poprawny pid";
        }
        catch(Exception e)
        {
            return "Niepoprawny pid";
        }

    }
}
