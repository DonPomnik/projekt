package pl.bergmann.projektczesc2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PracticalTipListener
{
    public List<ProcessOfComputer> processOfComputers = new ArrayList<>();
    private boolean newquery = false;

    @RabbitListener(queues = "procesy")
    public void consumeDefaultMessage(String s)
    {
        if(s.equals("end"))
        {
            newquery = true;
            return;
        }

        if(newquery)
        {
            processOfComputers = new ArrayList<>();
            newquery = false;
        }

        String keysValues = s.substring(s.indexOf('{'),s.indexOf('}'));
        String[] current = keysValues.split(",");
        System.out.println(s);
        List<String> values = new ArrayList<>();
        ProcessOfComputer p = new ProcessOfComputer();
        for(int i = 0;i<current.length;i++)
        {
            switch (i)
            {
                case 0: //name
                    String name = current[i].substring(current[i].indexOf('\''),current[i].indexOf('\'', current[i].indexOf('\'') + 1));
                    p.setName(name.substring(1));
                    break;
                case 1: //sessionName
                    String sessionName = current[i].substring(current[i].indexOf('\''),current[i].indexOf('\'', current[i].indexOf('\'') + 1));
                    p.setSessionName(sessionName.substring(1));
                    break;
                case 2: //status
                    String status = current[i].substring(current[i].indexOf('\''),current[i].indexOf('\'', current[i].indexOf('\'') + 1));
                    p.setStatus(status.substring(1));
                    break;
                case 3: // userName
                    String userName = current[i].substring(current[i].indexOf('\''),current[i].indexOf('\'', current[i].indexOf('\'') + 1));
                    p.setUserName(userName.substring(1));
                    break;
                case 4: //windowTitle
                    String windowTitle = current[i].substring(current[i].indexOf('\''),current[i].indexOf('\'', current[i].indexOf('\'') + 1));
                    p.setWindowTitle(windowTitle.substring(1));
                    break;
                case 5: //pid
                    String pid = current[i].substring(current[i].indexOf('='),current[i].length());
                    p.setPid(Integer.parseInt(pid.substring(1)));
                    break;
                case 6: //session
                    String session = current[i].substring(current[i].indexOf('='),current[i].length());
                    p.setSession(Integer.parseInt(session.substring(1)));
                    break;
                case 7: //memUsage
                    String memUsage = current[i].substring(current[i].indexOf('='),current[i].length());
                    p.setMemUsage(Integer.parseInt(memUsage.substring(1)));
                    break;
                case 8: //cpuTime
                    String cpuTime = current[i].substring(current[i].indexOf('\''),current[i].indexOf('\'', current[i].indexOf('\'') + 1));
                    p.setCpuTime(cpuTime.substring(1));
                    break;
            }

            /*
            // values.add(current[i].substring(current[i].indexOf('\''),current[i].indexOf('\'')));
            // values.add(current[i].substring(current[i].indexOf('=' +1),current[i].indexOf(',')));
            String vv = current[i];
            if(current[i].contains("'"))
            {

               // System.out.println("z is " + z);
            }
            else
            {

                values.add(x.substring(1));
                int value = Integer.parseInt(x);
                //System.out.println("x is " + value);
            }
            */

        }
        processOfComputers.add(p);
    }
}