package pl.bergmann.projektczesc1;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProcessApi
{
    @Test
    public void contextLoads2()
    {
        //given
        ProcessesGUI loggerApi=mock(ProcessesGUI.class);
        //when
        when(loggerApi.testPid("1")).thenReturn(testKolejny("1"));
        //then
        Assert.assertEquals(loggerApi.testPid("1"),"Poprawny pid");
    }


    public String testKolejny(String pid)
    {
        ProcessOfComputer processOfComputer = new ProcessOfComputer();
        try
        {
            processOfComputer.setPid(Integer.parseInt(pid));
            return "Poprawny pid";
        }
        catch(Exception e)
        {
            return "Niepoprawny pid";
        }
    }
}
