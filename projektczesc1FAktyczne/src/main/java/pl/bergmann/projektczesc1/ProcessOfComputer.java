package pl.bergmann.projektczesc1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ProcessOfComputer implements Serializable {
    private String name;
    private String sessionName;
    private String status;
    private String userName;
    private String windowTitle;
    private int pid;
    private int session;
    private int memUsage;
    private String cpuTime;

    @Override
    public String toString() {
        return "ProcessOfComputer{" +
                "name='" + name + '\'' +
                ", sessionName='" + sessionName + '\'' +
                ", status='" + status + '\'' +
                ", userName='" + userName + '\'' +
                ", windowTitle='" + windowTitle + '\'' +
                ", pid=" + pid +
                ", session=" + session +
                ", memUsage=" + memUsage +
                ", cpuTime='" + cpuTime + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWindowTitle() {
        return windowTitle;
    }

    public void setWindowTitle(String windowTitle) {
        this.windowTitle = windowTitle;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getMemUsage() {
        return memUsage;
    }

    public void setMemUsage(int memUsage) {
        this.memUsage = memUsage;
    }

    public String getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(String cpuTime) {
        this.cpuTime = cpuTime;
    }

    public ProcessOfComputer(@JsonProperty("name")  String name, @JsonProperty("sessionName")  String sessionName, @JsonProperty("status")  String status, @JsonProperty("userName")  String userName,@JsonProperty("windowTitle")   String windowTitle, @JsonProperty("pid")  int pid,@JsonProperty("session")   int session,@JsonProperty("memUsage")   int memUsage,@JsonProperty("cpuTime")  String cpuTime) {
        this.name = name;
        this.sessionName = sessionName;
        this.status = status;
        this.userName = userName;
        this.windowTitle = windowTitle;
        this.pid = pid;
        this.session = session;
        this.memUsage = memUsage;
        this.cpuTime = cpuTime;
    }



    public ProcessOfComputer() {
    }


}