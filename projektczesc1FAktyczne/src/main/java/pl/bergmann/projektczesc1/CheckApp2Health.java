package pl.bergmann.projektczesc1;


import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CheckApp2Health implements HealthIndicator {

    private RestTemplate restTemplate;

    @Autowired
    public CheckApp2Health(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Health health() {
        try {
            JsonNode resp = restTemplate.getForObject("http://localhost:9090/actuator/health", JsonNode.class);

            if (resp.get("status").asText().equalsIgnoreCase("UP")) {
                return Health.up().build();
            }

            return Health.outOfService().withDetail("Response status", resp.textValue()).build();
        } catch (Exception ex) {
            return Health.down(ex).build();
        }
    }
}