package pl.bergmann.projektczesc1;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class RetrofitClient {
    Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    public Retrofit getRetrofitClient()
    {
        OkHttpClient httpClient = new OkHttpClient();
        return new Retrofit.Builder()
                .baseUrl("http://localhost:9090/register/")
                .client(httpClient)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
    }
}