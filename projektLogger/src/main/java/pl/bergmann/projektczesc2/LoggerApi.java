package pl.bergmann.projektczesc2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class LoggerApi
{
    ProcessOfComputerRepo repo;

    @Autowired
    public LoggerApi(ProcessOfComputerRepo repo)
    {
        this.repo = repo;
    }

    @PostMapping("/register")
    public void register(@RequestBody ProcessOfComputer processOfComputer)
    {
        processOfComputer.setDateOfDeleting(new Date());
        repo.save(processOfComputer);
    }
}
