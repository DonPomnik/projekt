package pl.bergmann.projektczesc2;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class ProcessOfComputer
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;



    private String sessionName;
    private String status;
    private String userName;
    private String windowTitle;

    public Date getDateOfDeleting() {
        return dateOfDeleting;
    }

    public void setDateOfDeleting(Date dateOfDeleting) {
        this.dateOfDeleting = dateOfDeleting;
    }

    private Date dateOfDeleting;

    @Override
    public String toString() {
        return "ProcessOfComputer{" +
                "name='" + name + '\'' +
                ", sessionName='" + sessionName + '\'' +
                ", status='" + status + '\'' +
                ", userName='" + userName + '\'' +
                ", windowTitle='" + windowTitle + '\'' +
                ", pid=" + pid +
                ", session=" + session +
                ", memUsage=" + memUsage +
                ", cpuTime='" + cpuTime + '\'' +
                '}';
    }

    private int pid;
    private int session;
    private int memUsage;
    private String cpuTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProcessOfComputer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWindowTitle() {
        return windowTitle;
    }

    public void setWindowTitle(String windowTitle) {
        this.windowTitle = windowTitle;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getMemUsage() {
        return memUsage;
    }

    public void setMemUsage(int memUsage) {
        this.memUsage = memUsage;
    }

    public String getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(String cpuTime) {
        this.cpuTime = cpuTime;
    }

    public ProcessOfComputer(String name, String sessionName, String status, String userName, String windowTitle, int pid, int session, int memUsage, String cpuTime,Date dateOfDeleting) {
        this.name = name;
        this.sessionName = sessionName;
        this.status = status;
        this.userName = userName;
        this.windowTitle = windowTitle;
        this.pid = pid;
        this.session = session;
        this.memUsage = memUsage;
        this.cpuTime = cpuTime;
        this.dateOfDeleting = dateOfDeleting;
    }
}
