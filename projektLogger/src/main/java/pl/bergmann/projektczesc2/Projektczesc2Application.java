package pl.bergmann.projektczesc2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projektczesc2Application {

    public static void main(String[] args) {
        SpringApplication.run(Projektczesc2Application.class, args);
    }

}
