package pl.bergmann.projektczesc2;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessOfComputerRepo extends CrudRepository<ProcessOfComputer,Long>
{

}
